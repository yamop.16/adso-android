package com.nahtredn.utilities;

import android.util.ArrayMap;
import android.util.Log;

import com.nahtredn.helpers.Queries;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class Dgraph {

    private static String MUTATE = "mutate";
    private static String QUERY = "query";

    private static String DGRAPH_SERVER = "http://212.237.18.149:8080/";

    static public Map<String, String> queryCredentials(String username){
        String query = Queries.queryPasswordFor(username);
        String result = performQuery(query, QUERY);
        if (result != null) {
            JSONObject first = getFirstResult(result);
            if (first != null) {
                Map<String, String> credentials = new ArrayMap<>();
                try {
                    if (first.has("email")) {
                        credentials.put("email", first.getString("email"));
                    }

                    if (first.has("password")) {
                        credentials.put("password", first.getString("password"));
                    }

                    if (first.has("uid")) {
                        credentials.put("uid", first.getString("uid"));
                    }

                    return credentials;
                } catch (JSONException je){
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public static Map<String, String> queryRegistry(String username, String password){
        String query = Queries.queryCreateAccountFor(username, password);
        String result = performQuery(query, MUTATE);
        if(result != null){
            JSONObject insert = getInsertResult(result);
            if(insert != null){
                Map<String, String> registry = new ArrayMap<>();
                try {
                    if (insert.has("person")) {
                        registry.put("uid", insert.getString("person"));
                    }

                    registry.put("username", username);
                    registry.put("password", password);

                    return registry;
                } catch(JSONException je) {
                    je.printStackTrace();
                    return null;
                }
            } else {
                return null;
            }
        }
        return null;
    }

    private static String performQuery(String query, String endpoint){
        try {
            URL url = new URL(DGRAPH_SERVER + endpoint);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "text/plain");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("X-Dgraph-CommitNow", "true");
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream());
            dataOutputStream.writeBytes(query);
            dataOutputStream.flush();
            dataOutputStream.close();
            InputStream input = connection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(input, "utf-8"), 8);
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            input.close();
            return stringBuilder.toString();
        } catch (IOException ioe) {
            return null;
        }
    }

    private static JSONObject getFirstResult(String result){
        try {
            JSONObject mainObject = new JSONObject(result);
            JSONObject data = mainObject.getJSONObject("data");
            JSONArray res = data.getJSONArray("result");
            return res.length() > 0 ? res.getJSONObject(0) : null;
        } catch (JSONException je) {
            je.printStackTrace();
            return null;
        }
    }

    private static JSONObject getInsertResult(String result){
        try{
            JSONObject main = new JSONObject(result);
            JSONObject data = main.getJSONObject("data");
            JSONObject uids = data.getJSONObject("uids");
            return uids;
        } catch(JSONException je) {
            je.printStackTrace();
            return null;
        }
    }
}
