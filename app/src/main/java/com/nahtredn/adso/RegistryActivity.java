package com.nahtredn.adso;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.nahtredn.utilities.Dgraph;
import com.nahtredn.utilities.Messenger;
import com.nahtredn.utilities.PreferencesProperties;
import com.nahtredn.utilities.RealmController;
import com.nahtredn.utilities.Validator;

import java.util.Map;

public class RegistryActivity extends AppCompatActivity {

    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registry);
        getSupportActionBar().hide();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(intent);
            RegistryActivity.this.finish();
            return true;
        }

        return true;
    }

    public void registry(View view){

        EditText inputUsername = findViewById(R.id.username);
        TextInputLayout layoutUsername = findViewById(R.id.label_username);

        if (!Validator.with(this).validateEmail(inputUsername, layoutUsername)){
            return;
        }

        EditText inputPassword = findViewById(R.id.password);
        TextInputLayout layoutPassword = findViewById(R.id.label_password);

        if (!Validator.with(this).validateText(inputPassword, layoutPassword)){
            return;
        }

        EditText inputRepeatPassword = findViewById(R.id.repeat_password);
        TextInputLayout layoutRepeatPassword = findViewById(R.id.label_repeat_password);

        if (!Validator.with(this).validateText(inputRepeatPassword, layoutRepeatPassword)){
            return;
        }

        if (!inputPassword.getText().toString().trim().equals(inputRepeatPassword.getText().toString().trim())){
            Validator.with(this).setErrorMessage(inputRepeatPassword, layoutRepeatPassword, "La contraseña no coincide con la anterior");
            return;
        }

        showAlertTerms(inputUsername.getText().toString(), inputPassword.getText().toString());
    }

    public void showAlertTerms(final String email, final String password){

        AlertDialog.Builder ad = new AlertDialog.Builder(this);
        ad.setTitle("Terminos y condiciones");
        ad.setView(LayoutInflater.from(this).inflate(R.layout.terminos,null));

        ad.setPositiveButton("Acepto",
                new android.content.DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        new Registry().execute(email, password);
                    }
                }
        );
        ad.setNegativeButton("No acepto", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Messenger.with(RegistryActivity.this).showMessage("Para registrarte es necesario aceptar los términos y condiciones");
            }
        });

        ad.show();
    }

    private void logIn(String uid, String username, String password){
        RealmController.with(RegistryActivity.this).login(uid, username, password);
        Messenger.with(this).showMessage("Bienvenido");
        Intent intent = new Intent(RegistryActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getApplication().startActivity(intent);
        this.finish();
    }

    class Registry extends AsyncTask<String, String, String> {
        Map<String, String> registry;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(RegistryActivity.this, ProgressDialog.THEME_HOLO_DARK);
            pDialog.setMessage("Creando cuenta...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            registry = Dgraph.queryRegistry(params[0], params[1]);
            return null;
        }

        @Override
        protected void onPostExecute(String params) {
            pDialog.dismiss();
            if(registry != null){
                logIn(registry.get("uid"), registry.get("username"), registry.get("password"));
            } else {
                Messenger.with(RegistryActivity.this).showMessage("Error: Ya existe una cuenta con ese correo");
            }
        }
    }
}
