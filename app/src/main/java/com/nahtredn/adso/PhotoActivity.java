package com.nahtredn.adso;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.nahtredn.utilities.Images;
import com.nahtredn.utilities.Messenger;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PhotoActivity extends AppCompatActivity {
    private static final String photoPreferences = "photoPreferences";
    private static final String photoProperty = "photo";
    private static final String photoFileName = "profile_photo";
    private static final String photoFileExtension = "jpg";
    private static final String dirChild = "content";

    private InterstitialAd mInterstitialAd;
    private ImageView photo;

    private String mImageFileLocation = "";

    private static final int TAKE_PHOTO_REQUEST_CODE = 1;
    private static final int SELECT_PHOTO_REQUEST_CODE = 2;
    private static final int CROP_PHOTO_REQUEST_CODE = 3;
    private static final int WRITE_EXTERNAL_STORAGE_REQUEST_CODE = 4;
    private static final int CAMERA_REQUEST_CODE = 5;

    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        AdView mAdView = findViewById(R.id.adViewPhotoProfile);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mInterstitialAd = newInterstitialAd();
        loadInterstitial();

        photo = findViewById(R.id.image_photo);
        loadImage();
    }

    private void loadImage(){
        String pathPhoto = getImageUrl();
        if (pathPhoto != null){
            File imgFile = new  File(pathPhoto);
            if(imgFile.exists()){
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                photo.setImageBitmap(myBitmap);
            }
        } else {
            photo.setImageDrawable(getDrawable(R.drawable.ic_profile));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.common, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home){
            this.finish();
            return true;
        }

        if (id == R.id.action_delete){
            saveImageUrl(null);
            photo.setImageDrawable(getDrawable(R.drawable.ic_profile));
            showMessage(getString(R.string.photo_profile_deleted_activity_photo));
            return true;
        }

        if (id == R.id.action_rotate){
            try{
                UpdateOrientation();
            }catch(IOException ioe){
                ioe.printStackTrace();
            }
            return true;
        }

        if (id == R.id.action_help){
            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.commonAlertDialog));
            builder.setMessage(getString(R.string.help_photo_activity))
                    .setTitle(getString(R.string.help_title));
            AlertDialog dialog = builder.create();
            dialog.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void UpdateOrientation() throws IOException{
        String pathPhoto = getImageUrl();
        if (pathPhoto != null){
            File imgFile = new  File(pathPhoto);
            if(imgFile.exists()){
                Bitmap myBitmap = Images.rotateImage(BitmapFactory.decodeFile(imgFile.getAbsolutePath()), 90);
                FileOutputStream out = new FileOutputStream(imgFile);
                myBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                photo.setImageBitmap(myBitmap);
            }
        }
    }

    public void onClicButtonPhoto(View view){
        int code = getApplicationContext().getPackageManager().checkPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, getApplicationContext().getPackageName());
        if (code == PackageManager.PERMISSION_GRANTED) {
            showInterstitial();
            if (view.getId() == R.id.btn_take_picture_photo){
                launchActivityForUpdatePictureProfile(true);
                return;
            }
            if (view.getId() == R.id.btn_select_picture_photo){
                launchActivityForUpdatePictureProfile(false);
                return;
            }
        } else {
            showMessage("ADSO requiere permisos de escritura para crear y guardar la foto de perfil y la solicitud digital.");
            ActivityCompat.requestPermissions(PhotoActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    WRITE_EXTERNAL_STORAGE_REQUEST_CODE);
        }
    }

    private InterstitialAd newInterstitialAd() {
        InterstitialAd interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getString(R.string.interstitial_ad_unit_id));
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
            }

            @Override
            public void onAdClosed() {
                reloadInterstitial();
            }
        });
        return interstitialAd;
    }

    private void showInterstitial() {
        if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            reloadInterstitial();
        }
    }

    private void loadInterstitial() {
        AdRequest adRequest = new AdRequest.Builder().build();
        mInterstitialAd.loadAd(adRequest);
    }

    private void reloadInterstitial() {
        mInterstitialAd = newInterstitialAd();
        loadInterstitial();
    }

    File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "IMAGE_" + timeStamp + "_";
        File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName,".jpg", storageDirectory);
        mImageFileLocation = image.getAbsolutePath();
        return image;
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Log.w("Photo activity","Código recibido: " + requestCode);
        switch (requestCode) {
            case WRITE_EXTERNAL_STORAGE_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showMessage("Excelente, ADSO ya puede crear los archivos necesarios para su funcionamiento");
                } else {
                    showMessage("Sin permisos de escritura, ADSO no puede funcionar");
                }
                break;
            case CAMERA_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showMessage("Excelente, ADSO ya puede acceder a la camara para tomar la foto de perfil");
                } else {
                    showMessage("Sin permiso para usar la camara, ADSO no puede tomar la foto de perfil");
                }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void launchActivityForUpdatePictureProfile(boolean takePicture) {
        if (takePicture) {
            int code = getApplicationContext().getPackageManager().checkPermission(Manifest.permission.CAMERA, getApplicationContext().getPackageName());
            if (code == PackageManager.PERMISSION_GRANTED) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    File photoFile = null;
                    try{
                        photoFile = createImageFile();
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, getUriFrom(photoFile));
                        startActivityForResult(takePictureIntent, TAKE_PHOTO_REQUEST_CODE);
                    } catch (IOException ioe) {
                        showMessage("Error al tratar de utilizar la camara.");
                    }
                    startActivityForResult(takePictureIntent, TAKE_PHOTO_REQUEST_CODE);
                }
            } else{
                showMessage("ADSO requiere permisos para acceder a la cámara y tomar la foto de perfil");
                ActivityCompat.requestPermissions(PhotoActivity.this,
                        new String[]{Manifest.permission.CAMERA},
                        CAMERA_REQUEST_CODE);
            }
        } else {
            Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");
            startActivityForResult(intent, SELECT_PHOTO_REQUEST_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case (TAKE_PHOTO_REQUEST_CODE):
                if (resultCode != RESULT_OK) {
                    return;
                }
                setReducedImageSize();
                break;
            case (SELECT_PHOTO_REQUEST_CODE):
                if (resultCode != RESULT_OK) {
                    return;
                }
                Uri uri = data.getData();
                if (uri.toString().startsWith("content://com.google.android.apps.photos")){
                    new DownloadImage(uri).execute();
                } else {
                    startCrop(uri);
                }
                break;
            case (CROP_PHOTO_REQUEST_CODE):
                if (resultCode != RESULT_OK){
                    return;
                }
                Bundle extras = data.getExtras();
                if (extras == null){
                    showMessage(getString(R.string.error_croped_photo));
                    return;
                }
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                photo.setImageBitmap(imageBitmap);
                String fileName = saveImageFile(imageBitmap);
                saveImageUrl(fileName);
                break;
        }
    }

    void setReducedImageSize() {
        int targetImageViewWidth = photo.getWidth();
        int targetImageViewHeight = photo.getHeight();

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mImageFileLocation, bmOptions);
        int cameraImageWidth = bmOptions.outWidth;
        int cameraImageHeight = bmOptions.outHeight;

        int scaleFactor = Math.min(cameraImageWidth/targetImageViewWidth, cameraImageHeight/targetImageViewHeight);
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inJustDecodeBounds = false;

        Bitmap photoReducedSizeBitmp = BitmapFactory.decodeFile(mImageFileLocation, bmOptions);
        String fileName = saveImageFile(photoReducedSizeBitmp);
        saveImageUrl(fileName);
        photo.setImageBitmap(photoReducedSizeBitmp);
    }

    /**
     * start Crop
     *
     * @param uri image uri
     */
    private void startCrop(Uri uri){
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("aspectX", 4);
        intent.putExtra("aspectY", 4);
        intent.putExtra("scaleUpIfNeeded", true);
        intent.putExtra("scale", "true");
        intent.putExtra("return-data", true);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        try {
            startActivityForResult(intent, CROP_PHOTO_REQUEST_CODE);
        } catch (SecurityException s){
            showMessage(getString(R.string.error_selected_photo));
        }
    }

    private Uri getUriFrom(File imageFile){
        String authorities = getApplicationContext().getPackageName() + ".provider";
        Log.w("Photo activity","authorities: " + authorities);
        Log.w("Photo activity","imageFile " + imageFile);
        return FileProvider.getUriForFile(this, authorities, imageFile);
    }

    public String saveImageFile(Bitmap bitmap) {
        FileOutputStream out;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filename;
    }

    private String getFilename() {
        File file = new File(getFilesDir()
                .getPath(), dirChild);
        file.mkdirs();
        return  (file.getAbsolutePath() + "/" + photoFileName + "." + photoFileExtension);
    }

    private void saveImageUrl(String pathPhoto){
        SharedPreferences prefs = getSharedPreferences(photoPreferences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(photoProperty, pathPhoto);
        editor.apply();
        editor.clear();
    }

    private String getImageUrl(){
        SharedPreferences prefs = getSharedPreferences(photoPreferences,Context.MODE_PRIVATE);
        return prefs.getString(photoProperty, null);
    }

    private void showMessage(String message){
        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
    }

    class DownloadImage extends AsyncTask<String, String, String> {

        private Uri uri;
        private File file;

        DownloadImage(Uri uri){
            this.uri = uri;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(PhotoActivity.this, ProgressDialog.THEME_HOLO_DARK);
            pDialog.setMessage("Descargando imagen desde Google Fotos...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                FileOutputStream out;
                InputStream is = getContentResolver().openInputStream(uri);
                if (is != null) {
                    Bitmap pictureBitmap = BitmapFactory.decodeStream(is);
                    file = createImageFile();
                    out = new FileOutputStream(file);
                    pictureBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                showMessage("Error: " + e.getMessage());
            } catch (IOException ioe){
                showMessage("Error: " + ioe.getMessage());
                ioe.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            pDialog.dismiss();
            if (file != null) {
                startCrop(getUriFrom(file));
            }
        }
    }
}
